#The Hub

Download selenium server 2.42.0 into /home/paul
```````````
wget http://selenium-release.storage.googleapis.com/2.43/selenium-server-standalone-2.43.1.jar
```````````

Copy the startup script and make it executable:

```````````
sudo cp init.d/selenium-hub.sh /etc/init.d/selenium-hub
sudo chmod a+x /etc/init.d/selenium-hub

```````````
Test the startup script
```````````
cd /etc/init.d
sudo selenium-hub start
```````````
Register the init script
```````````
sudo update-rc.d selenium-hub defaults
```````````

Reboot via Digital Ocean power cycle and check that the hub is running
```````````
http://SERVERIP:4444/grid/console
```````````

To restart the hub, do a powercycle on DO droplet panel

Logs are here:
```````````
/var/log/boot.log
```````````

#Nodes

1. Spin up a new docker instance on DigitalOcean (NYC2) (see info.md) or SSH into an existing one
2. Login to dockerhub
3. Pull/run down the docker selenium node image

```````````
docker login
export PORT=`shuf -i 49153-65535 -n 1` && docker run -d -p $PORT:$PORT -e "HOST_IP=`/sbin/ifconfig eth1 | grep "inet addr" | awk -F: '{print $2}' | awk '{print $1}'`" -e "PORT=$PORT" pauljohncleary/selenium-node
```````````

- export PORT creates a random port number for us to use (we then pass it in using -e as an env var PORT)
- -d detaches the container
- -p maps the random port to this container
- the other -e and the command passes through the host IP

The docker selenium hub is an Ubuntu 14.04 base image which has:

- JRE Headless, Git and Wget installed
- A freshly cloned selenium config git repo, 
- PhantomJS
- Selenium Standalone JAR 
- A CMD command to start the node

To restart the node, get the id and restart it
```````````
docker ps
docker restart <ID>
```````````
To look at the logs
```````````
docker logs <ID>
```````````