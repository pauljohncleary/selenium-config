#! /bin/sh

### BEGIN INIT INFO
# Provides:		selenium-hub
# Required-Start:	$remote_fs $syslog
# Required-Stop:	$remote_fs $syslog
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description:	Selenium Hub
### END INIT INFO

SELENIUM_DIR=/home/paul
SELENIUM_VERSION=2.43.1

case "$1" in
  start)
    java -jar $SELENIUM_DIR/selenium-server-standalone-$SELENIUM_VERSION.jar -role hub -hubConfig $SELENIUM_DIR/selenium-config/selenium-hub.json
    ;;
 stop)
    pid=`ps aux | grep selenium-[s]erver | awk '{print $2}'`
    kill -9 $pid
    sleep 10
   ;;
 restart)
    pid=`ps aux | grep selenium-[s]erver | awk '{print $2}'`
    kill -9 $pid
    sleep 20
    java -jar $SELENIUM_DIR/selenium-server-standalone-$SELENIUM_VERSION.jar -role hub -hubConfig $SELENIUM_DIR/selenium-config/selenium-hub.json
   ;;
 *)
   echo "Usage: selenium-hub {start|stop|restart}" >&2
   exit 3
   ;;
esac
