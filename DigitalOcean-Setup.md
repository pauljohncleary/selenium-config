#Digital Ocean Setup

##To create a droplet:
- NYC2 - Enable Private Networking
- Choose an image (usually ubuntu 14.04, Docker or Dokku)
- Select SSH keys (pauljohncleary@gmail.com)

##To generate SSH Keys
````````
ssh-keygen -t rsa
````````
- Choose a name for the key
- Copy and paste the .pub key to the Digital Ocean SSH key setup

##To ssh into the droplet
````````
ssh root@DROPLET-IP
````````
Where you have multiple identities:
````````
ssh -i ~/.ssh/SSH-KEY-NAME root@DROPLET-IP
````````

##Setup docker container access
````````
docker run -v /usr/local/bin:/target jpetazzo/nsenter
````````
Then to enter a docker container we can do:
````````
docker ps
docker-enter <container-id>
````````

## Basic security
https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-14-04

##Setup a swap file, by default there isn't one available
https://www.digitalocean.com/community/tutorials/how-to-add-swap-on-ubuntu-14-04