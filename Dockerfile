FROM ubuntu

MAINTAINER Paul Cleary "pauljohncleary@gmail.com"

# Env
ENV PHANTOMJS_VERSION 1.9.7
ENV DISPLAY :99

# Install deps
RUN apt-get update -y
RUN apt-get install -y -q \
  git \
  wget \
  openjdk-7-jre-headless \
  firefox \
  xvfb \
  xfonts-100dpi \
  xfonts-75dpi \
  xfonts-scalable \
  xfonts-cyrillic \
  unzip

# Get the selenium standalone server
RUN wget http://selenium-release.storage.googleapis.com/2.43/selenium-server-standalone-2.43.1.jar

# Get PhantomJS
RUN wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-1.9.7-linux-x86_64.tar.bz2

# Set up PhantomJS
RUN \
  mkdir -p /srv/var && \
  wget -q --no-check-certificate -O /tmp/phantomjs-$PHANTOMJS_VERSION-linux-x86_64.tar.bz2 https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-$PHANTOMJS_VERSION-linux-x86_64.tar.bz2 && \
  tar -xjf /tmp/phantomjs-$PHANTOMJS_VERSION-linux-x86_64.tar.bz2 -C /tmp && \
  rm -f /tmp/phantomjs-$PHANTOMJS_VERSION-linux-x86_64.tar.bz2 && \
  mv /tmp/phantomjs-$PHANTOMJS_VERSION-linux-x86_64/ /srv/var/phantomjs && \
  ln -s /srv/var/phantomjs/bin/phantomjs /usr/bin/phantomjs

# Test PhantomJS installation
RUN phantomjs --version

# Create a user to run 
RUN useradd -d /home/seleuser -m seleuser

# Clone the settings repo
RUN git clone https://bitbucket.org/pauljohncleary/selenium-config.git /home/seleuser/selenium-config

# Start the node
# CMD statements only run when the container is started/restart, HOST_IP / PORT are provided as an ENV vars at run time
CMD cd /home/seleuser/selenium-config && git pull && \
  export DISPLAY=:99  && \
  Xvfb :99 -screen 0 1366x768x16 & \
  su - seleuser -c -- "java -jar /selenium-server-standalone-2.43.1.jar -role node -nodeConfig ~/selenium-config/selenium-node.json -host $HOST_IP -port $PORT"
